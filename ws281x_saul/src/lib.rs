#![feature(maybe_uninit_ref)]
#![feature(maybe_uninit_extra)]
#![feature(pin_static_ref)]
#![no_std]

use core::mem::MaybeUninit;
use core::pin::Pin;

use riot_wrappers::mutex::Mutex;
use riot_wrappers::saul::{Class, Drivable, Driver, Phydat, Registration};
use riot_wrappers::ws281x::*;

// Only used through autoinit, shouldn't occupy memory if that's unused (but guarding with
// with-autoinit anyway because NUMOF may not be defined otherwise).
#[cfg(feature = "with-autoinit")]
static WSS: Mutex<Option<Ws281xSaul<'static, GRBW, { riot_sys::WS281X_PARAM_NUMOF as _ }>>> =
    Mutex::new(None);

/// Create a single buffered WS281x strand (currently hard-configured to GRBW LEDs) of
/// WS281X_PARAM_NUMOF items at pin WS281X_PARAM_PIN.
///
/// The buffer is statically allocated, and leaked (ie. not reclaimable; it's running until the
/// next reboot).
///
/// By wrapping this in a `#[no_mangle] pub extern "C"` function, it can be called from auto_init
/// -- but that is all done more easily when XFA is usable there.
#[cfg(feature = "with-autoinit")]
pub fn autoinit() {
    // Should really happen in the board
    let c_pin = riot_sys::WS281X_PARAM_PIN;
    let pin = riot_wrappers::gpio::GPIO::from_c(c_pin as _).unwrap();

    let ws = BufferedWs281x::<_, { riot_sys::WS281X_PARAM_NUMOF as _ }>::init(pin);
    let mut wss = WSS.try_lock().expect("Double initialization");
    *wss = Some(Ws281xSaul::new(ws));
    let shortref = wss.as_mut().unwrap();
    // unsafe: Reborrow as static is OK because it's a static mutex and by dropping the lock we
    // ensure no other party can ever lock it again and gain access.
    let staticref: &'static mut Ws281xSaul<_, { riot_sys::WS281X_PARAM_NUMOF as _ }> =
        unsafe { &mut *(shortref as *mut _) };
    // Ensure nobody else will ever use it
    core::mem::forget(wss);
    core::pin::Pin::static_mut(staticref).register();
}

/// A WS281x string that is made available through SAUL.
///
/// Using it through SAUL is not the most memory efficient because there are individual registries
/// (with two extra pointers to split things up between the strand and the index) and names around
/// -- but hey it's easy to use, and convenient for short strings. (Ever seeing this used for
/// matrix displays will make the author very sad).
///
/// This writes to the WS strand whenever an entry is written to, and from the thread (or
/// interrupt, yikes) that does it. That's not pretty, but again, simple. (Later extension could
/// add a tri-state for "write immediately", "delay writes and nothing did actually change" and
/// "Delay writes and something did change" could be added, and an alternative implementation in
/// which writes happen in a dedicated clocked high-priority thread is feasible).
///
/// By employing an internal mutex, this satisifies the SAUL requirements of being callable from
/// everywhere.
pub struct Ws281xSaul<'a, C: ChannelType + SingleDrivable, const N: usize> {
    ws: Mutex<BufferedWs281x<C, N>>,
    initialized: bool,

    // Space where the SAUL entries can live forever once the object is pinned
    //
    // Both use MaybeUninit because initializing a None generic array is still a mess, and
    // individual options would be wasteful.
    fatpointers: [MaybeUninit<FatPointer<'a, C, N>>; N],
    regs: [MaybeUninit<Registration<'a, FatPointer<'a, C, N>>>; N],

    // By building the driver manually, we could in theory spare the FatPointer (by having custom
    // functions that pass the information about the chain and only leave the index for the SAUL
    // device; not worth it for a first iteration)
    driver: Driver<FatPointer<'a, C, N>>,
}

impl<'a, C: ChannelType + SingleDrivable, const N: usize> Ws281xSaul<'a, C, N> {
    pub fn new(ws: BufferedWs281x<C, N>) -> Self {
        Self {
            ws: Mutex::new(ws),
            initialized: false,
            // Both: Arrays don't have any data other than their inner uninits
            fatpointers: unsafe { MaybeUninit::uninit().assume_init() },
            regs: unsafe { MaybeUninit::uninit().assume_init() },
            driver: FatPointer::build_driver(),
        }
    }

    pub fn register(self: Pin<&'a mut Self>) {
        // We'd be overwriting the regs that are alredy committed to SAUL
        assert!(self.initialized == false, "Duplicate registration");

        // unsafe: OK because we're careful not to move anything out of self
        let s = unsafe { Pin::into_inner_unchecked(self) };

        for (i, p) in s.fatpointers.iter_mut().enumerate() {
            p.write(FatPointer {
                chain: &s.ws,
                index: i,
            });
        }

        for (i, reg) in s.regs.iter_mut().enumerate() {
            // unsafe: OK because was just initialized
            let reg = reg.write(Registration::new(
                &s.driver,
                unsafe { s.fatpointers[i].assume_init_ref() },
                None,
            ));
            // unsafe: OK because self is pinned
            let reg = unsafe { Pin::new_unchecked(reg) };
            reg.register();
        }

        s.initialized = true;
    }
}

/// A registry entry has just a single registry entry for a pointer to the device; as we need both
/// the whole device referenced and the to-be-manipulated index in there, this is the indirection
/// point that "packs" two values into a single void pointer.
///
/// The Copy/Clone and the option to have None in the chain is primarily to make the const
/// initialization of the array easy and safe.
#[derive(Copy, Clone)]
struct FatPointer<'a, C: ChannelType + 'a, const N: usize> {
    chain: &'a Mutex<BufferedWs281x<C, N>>,
    index: usize,
}

impl<'a, C: ChannelType + SingleDrivable, const N: usize> Drivable for FatPointer<'a, C, N> {
    fn read(&self) -> Result<Phydat, ()> {
        let mut chain = self.chain.lock();
        chain.buffer[self.index].read()
    }
    fn write(&self, data: &Phydat) -> Result<u8, ()> {
        let mut chain = self.chain.lock();
        let ret = chain.buffer[self.index].write(data);
        chain.write();
        ret
    }
    fn class() -> Class {
        // just because all are
        use riot_wrappers::saul::{ActuatorClass::LedRgb, Class::Actuator};
        Actuator(Some(LedRgb))
    }
}

/// Similar to Drivable, but referring to a single element in a larger component where the
/// mutability guard is handled by the encapsulating mechanism, and the write can thus already take
/// a mutable reference.
// The Send requirement makes it easy to build the Ws281xSaul and place it in static storage
pub trait SingleDrivable: Send {
    fn read(&self) -> Result<Phydat, ()>;
    fn write(&mut self, data: &Phydat) -> Result<u8, ()>;
}

/// This implements the RGB SAUL interface for RGBW by subtituting any amount of light on all
/// channels with white light. This falls short quite spectacularly by the W being much brighter --
/// just showing that the RGB cube is ill suited to express the RGBW color space.
impl SingleDrivable for GRBW {
    fn read(&self) -> Result<Phydat, ()> {
        let (r, g, b, w) = self.rgbw();
        Ok(Phydat::new(
            &[(r + w) as _, (g + w) as _, (b + w) as _],
            None,
            0,
        ))
    }
    fn write(&mut self, data: &Phydat) -> Result<u8, ()> {
        use core::cmp::min;
        if let &[r, g, b] = data.value() {
            let w = min(min(r, g), b);
            self.set_rgbw((r - w) as _, (g - w) as _, (b - w) as _, w as _);
            Ok(3)
        } else {
            Err(())
        }
    }
}

impl SingleDrivable for GRB {
    fn read(&self) -> Result<Phydat, ()> {
        let (r, g, b) = self.rgb();
        Ok(Phydat::new(&[r as _, g as _, b as _], None, 0))
    }
    fn write(&mut self, data: &Phydat) -> Result<u8, ()> {
        if let &[r, g, b] = data.value() {
            self.set_rgb(r as _, g as _, b as _);
            Ok(3)
        } else {
            Err(())
        }
    }
}

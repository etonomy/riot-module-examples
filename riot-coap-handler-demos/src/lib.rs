//! CoAP resources for various features of RIOT OS
//!
//! See the [rust-gcoap](https://github.com/RIOT-OS/RIOT/tree/master/examples/rust-gcoap) example
//! for how to use them.
#![no_std]
#![cfg_attr(feature = "nightly_docs", feature(doc_auto_cfg))]
#![doc = document_features::document_features!()]

mod common;

mod minicbor_helpers;

#[cfg(feature = "gpio")]
pub mod gpio;
#[cfg(feature = "riot-wrappers")]
pub mod graphics;
#[cfg(feature = "i2c")]
pub mod i2c;
#[cfg(feature = "riot-wrappers")]
pub mod led;
#[cfg(feature = "riot-wrappers")]
pub mod netif;
#[cfg(feature = "nib")]
pub mod nib;
#[cfg(feature = "ping")]
pub mod ping;
#[cfg(feature = "ping")]
pub mod ping_passive;
#[cfg(feature = "riot-wrappers")]
pub mod ps;
#[cfg(feature = "saul")]
pub mod saul;
#[cfg(feature = "riot-wrappers")]
pub mod stdio;
#[cfg(feature = "vfs")]
pub mod vfs;

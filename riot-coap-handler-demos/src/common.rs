#![cfg(any(feature = "i2c", feature = "gpio"))]

use coap_message::{error::RenderableOnMinimal, Code, MinimalWritableMessage};

// Until https://tools.ietf.org/html/draft-ietf-core-problem-details-01 is used
#[derive(Debug)]
pub(crate) struct ErrorDetail(pub(crate) &'static str);

impl RenderableOnMinimal for ErrorDetail {
    type Error<IE: RenderableOnMinimal + core::fmt::Debug> = IE;
    fn render<M: MinimalWritableMessage>(self, msg: &mut M) -> Result<(), M::UnionError> {
        msg.set_code(Code::new(coap_numbers::code::BAD_REQUEST)?);
        let _ = msg.set_payload(self.0.as_bytes());
        Ok(())
    }
}

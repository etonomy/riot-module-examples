[package]
name = "riot-coap-handler-demos"
version = "0.2.0"
authors = ["chrysn <chrysn@fsfe.org>"]
edition = "2021"
license = "MIT OR Apache-2.0"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
coap-handler = "0.2.0"
coap-handler-implementations = "0.5.2"
coap-message = "0.3"
coap-message-utils = "0.3.1"
coap-numbers = { version = "*", default-features = false }
riot-wrappers = { version = "0.9.0", optional = true }
riot-sys = { version = "*", optional = true } # only needed by vfs for detailed error handling; the constants should be available in all -sys versions usable with the wrappers
serde = { version = "*", default-features = false }
heapless = { version = "0.7", features = [ "serde" ] }
embedded-hal = "1.0"
serde_cbor = { version = "*", default-features = false }
embedded-graphics = "0.6"
serde_bytes = { version = "0.10", default-features = false }
# really, the very version that the embdeded-nal that riot-wrappers uses re-exports
no-std-net = { version = "0.5", optional = true }
hex = { version = "^0.4.3", default-features = false, optional = true }
riot-shell-commands = { git = "https://gitlab.com/etonomy/riot-module-examples", default-features = false, features = ["saul"], optional = true }
minicbor = { version = "0.24.4", features = [ "derive" ] }
ciborium-ll = { version = "0.2", optional = true }
ciborium-io = { version = "0.2", optional = true }
document-features = "0.2"
switch-hal = "0.4.0"
try-lock = "0.2.5"
embedded-hal-async = "1.0.0"

[features]
#! ## Feature flags

default = ["riot-wrappers"]

## Build everything that is built into RIOT
riot-wrappers = [ "dep:riot-wrappers", "dep:riot-sys" ]

## Build the [vfs] module
vfs = [ "riot-wrappers" ]
## Build the [i2c] module
i2c = [ "riot-wrappers" ]
## Build the [gpio] module
gpio = []
## Build the [ping] module
ping = [ "riot-wrappers", "no-std-net", "riot-wrappers/with_embedded_nal" ] # embedded_nal is required for the address conversion
## Build the [saul] module
# The ciborium versions are not used concurrently with minicbor, but are easy enough to swap around in the source code.
saul = [ "riot-wrappers", "hex", "riot-shell-commands", "ciborium-io", "ciborium-ll" ]
## Build the [nib] module
nib = [ "riot-wrappers" ]

## Enable documentation enhancements that depend on nightly
##
## Currently, this just enables doc_auto_cfg.
nightly_docs = []

This repository contains modules that make exemplary use of RIOT's Rust
bindings. See [riot-examples] for further documentation.

[riot-examples]: https://gitlab.com/etonomy/riot-examples
